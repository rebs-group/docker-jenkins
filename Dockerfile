FROM jenkins/jenkins:lts

MAINTAINER Felix Kerekes <felix@rebs-group.com>

USER root

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y --no-install-recommends jq libltdl7 php-cli php-curl python-pip && rm -rf /var/lib/apt/lists/*

RUN pip install virtualenv

WORKDIR /var

# Install docker-compose
RUN curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose

# Install arcanist
RUN git clone https://github.com/phacility/libphutil.git --branch stable
RUN git clone https://github.com/phacility/arcanist.git --branch stable

WORKDIR /

# Add arcanist to PATH
RUN ln -s /var/arcanist/bin/arc /usr/bin

# Grab gosu for easy step-down from root
ADD https://github.com/tianon/gosu/releases/download/1.5/gosu-amd64 /usr/local/bin/gosu
RUN chmod 755 /usr/local/bin/gosu

# NOTE: Add a `docker` group with the same ID as on the host. This is required
# in order to have access to the host's docker socket. Add the jenkins user to the docker group.
#
# docker group ID on ubuntu-docker-17.03.1-ce is 999
RUN groupadd -g 999 docker; usermod -a -G docker jenkins

USER jenkins

